package br.com.massuda.alexander.usuario.dao.finder.usuario.academico.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.finder.usuario.academico.IFinderAreaDeEstudo;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.AreaDeEstudo;

@Component
public class FinderAreaDeEstudoImpl extends Finder<AreaDeEstudo> implements IFinderAreaDeEstudo {

	public FinderAreaDeEstudoImpl() {
		super(AreaDeEstudo.class);
	}

	public AreaDeEstudo pesquisar(Long id) {
		AreaDeEstudo o = null;
		o = super.pesquisar(id);
		return o;
	}
	
	public List<AreaDeEstudo> pesquisarPorNomeComo(String nome) {
		List<AreaDeEstudo> lista = new ArrayList<AreaDeEstudo>();
		lista = super.pesquisarPorNomeComo(nome);
		return lista;
	}

	public List<AreaDeEstudo> listar() {
		List<AreaDeEstudo> lista = new ArrayList<AreaDeEstudo>();
		lista = super.listar();
		return lista;
	}

}
