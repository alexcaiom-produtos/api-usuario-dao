package br.com.massuda.alexander.usuario.dao.finder.usuario.academico;

import br.com.massuda.alexander.persistencia.IFinder;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.AtividadeEscolar;

public interface IFinderAtividadeEscolar extends IFinder<Long, AtividadeEscolar> {
	
}
