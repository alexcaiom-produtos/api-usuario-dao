package br.com.massuda.alexander.usuario.dao.impl;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.usuario.dao.ISistemaDAO;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Sistema;

@Component
public class SistemaDAOImpl extends DAO<Sistema>
							implements ISistemaDAO {

	public SistemaDAOImpl() {
		super(Sistema.class);
	}

}
