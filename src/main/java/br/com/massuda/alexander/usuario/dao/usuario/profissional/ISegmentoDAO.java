package br.com.massuda.alexander.usuario.dao.usuario.profissional;

import br.com.massuda.alexander.persistencia.IDAO;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Segmento;

public interface ISegmentoDAO extends IDAO<Segmento> {

}
