package br.com.massuda.alexander.usuario.dao.impl;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.usuario.dao.IPerfilDAO;
import br.com.massuda.alexander.usuario.orm.modelo.Perfil;

@Component
public class PerfilDAOImpl extends DAO<Perfil> implements IPerfilDAO {

	public PerfilDAOImpl() {
		super(Perfil.class);
	}


}
