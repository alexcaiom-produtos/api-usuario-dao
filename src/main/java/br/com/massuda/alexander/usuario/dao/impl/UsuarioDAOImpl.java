package br.com.massuda.alexander.usuario.dao.impl;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

@Component
public class UsuarioDAOImpl extends DAO<Usuario> {

	public UsuarioDAOImpl() {
		super(Usuario.class);
	}
	
}
