package br.com.massuda.alexander.usuario.dao.finder.usuario;

import java.util.List;

import br.com.massuda.alexander.persistencia.IFinder;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.Usuario;

public interface IFinderUsuario extends IFinder<Long, Usuario> {
	
	public Usuario pesquisarPorLogin (String login);
	public List<Usuario> pesquisarPorLoginComo (String login);
	public List<Usuario> pesquisarPorNomeComo(String nome);
	
}
