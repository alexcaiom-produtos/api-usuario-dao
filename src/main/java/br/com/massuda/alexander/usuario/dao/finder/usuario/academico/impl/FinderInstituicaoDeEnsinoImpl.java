package br.com.massuda.alexander.usuario.dao.finder.usuario.academico.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.persistencia.jdbc.factory.GeradorSQLBeanFactory;
import br.com.massuda.alexander.persistencia.jdbc.utils.TipoOperacao;
import br.com.massuda.alexander.usuario.dao.finder.impl.Finder;
import br.com.massuda.alexander.usuario.dao.finder.usuario.academico.IFinderInstituicaoDeEnsino;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.InstituicaoDeEnsino;

@Component
public class FinderInstituicaoDeEnsinoImpl extends Finder<InstituicaoDeEnsino> implements IFinderInstituicaoDeEnsino {
	
	public FinderInstituicaoDeEnsinoImpl() {
		this(TipoOperacao.NORMAL);
	}

	public FinderInstituicaoDeEnsinoImpl(TipoOperacao tipo) {
		super(InstituicaoDeEnsino.class);
		this.tipoOperacao = tipo;
	}

	public InstituicaoDeEnsino pesquisar(Long id) {
		InstituicaoDeEnsino o = null;
		o = super.pesquisar(id);
		return o;
	}
	
	public List<InstituicaoDeEnsino> pesquisarPorNomeComo(String nome) {
		List<InstituicaoDeEnsino> objs = new ArrayList<InstituicaoDeEnsino>();
		objs = super.pesquisarPorNomeComo(nome);
		return objs;
	}

	public List<InstituicaoDeEnsino> listar() {
//		List<InstituicaoDeEnsino> objs = new ArrayList<InstituicaoDeEnsino>();
//	
//		String sql = GeradorSQLBeanFactory.get(entidade).getComandoSelecao();
//		try {
//			resultados = pesquisarSemParametro(sql);
//			
//			while (resultados.next()) {
//				InstituicaoDeEnsino o = new InstituicaoDeEnsino();
//				super.preencher(o);
//				objs.add(o);
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
		
		return super.listar();
	}

}
