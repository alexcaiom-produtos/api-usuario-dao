package br.com.massuda.alexander.usuario.dao.usuario.profissional.impl;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.massuda.alexander.usuario.dao.usuario.profissional.IExperienciaProfissionalDAO;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.ExperienciaProfissional;

@Component
public class ExperienciaProfissionalDAOImpl extends DAO<ExperienciaProfissional> implements IExperienciaProfissionalDAO {

	public ExperienciaProfissionalDAOImpl() {
		super(ExperienciaProfissional.class);
	}

}
