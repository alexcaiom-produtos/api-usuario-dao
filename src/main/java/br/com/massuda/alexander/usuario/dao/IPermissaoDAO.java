package br.com.massuda.alexander.usuario.dao;

import br.com.massuda.alexander.persistencia.IDAO;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.autorizacao.Permissao;

public interface IPermissaoDAO extends IDAO<Permissao> {

}
