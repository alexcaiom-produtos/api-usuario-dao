package br.com.massuda.alexander.usuario.dao.impl;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.usuario.dao.IModuloDAO;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Modulo;

@Component
public class ModuloDAOImpl extends DAO<Modulo>
							implements IModuloDAO {

	public ModuloDAOImpl() {
		super(Modulo.class);
	}
	
}
