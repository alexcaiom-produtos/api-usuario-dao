package br.com.massuda.alexander.usuario.dao.usuario.academico;

import br.com.massuda.alexander.persistencia.IDAO;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.academico.AreaDeEstudo;

public interface IAreaDeEstudoDAO extends IDAO<AreaDeEstudo> {

}
