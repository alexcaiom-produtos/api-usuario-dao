package br.com.massuda.alexander.usuario.dao.impl;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.usuario.dao.IPermissaoDAO;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.autorizacao.Permissao;

@Component
public class PermissaoDAOImpl extends DAO<Permissao>
							implements IPermissaoDAO {

	public PermissaoDAOImpl() {
		super(Permissao.class);
	}
	
}
