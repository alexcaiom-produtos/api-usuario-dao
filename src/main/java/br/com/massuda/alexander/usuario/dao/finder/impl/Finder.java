package br.com.massuda.alexander.usuario.dao.finder.impl;

import java.util.Objects;

import br.com.massuda.alexander.persistencia.jdbc.enums.JDBCDriver;
import br.com.massuda.alexander.persistencia.jdbc.finder.FinderJDBC;
import br.com.massuda.alexander.persistencia.jdbc.utils.ConstantesPersistencia;
import br.com.massuda.alexander.usuario.dao.impl.DAO;

public class Finder<T> extends FinderJDBC<T> {

	public Finder(Class<T> entidade) {
		super(entidade);
		DAO.configurarBancoDeDadosDadosAcesso();
	}
	

	public Finder(Class<T> entidade, String tipoBD) {
		super(entidade);
		driver = JDBCDriver.get(tipoBD);

		if (Objects.nonNull(driver)) {
			ConstantesPersistencia.BD_DRIVER = driver;
		}
		DAO.configurarBancoDeDadosDadosAcesso();
	}

}
