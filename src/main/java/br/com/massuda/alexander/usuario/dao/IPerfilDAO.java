package br.com.massuda.alexander.usuario.dao;

import br.com.massuda.alexander.persistencia.IDAO;
import br.com.massuda.alexander.usuario.orm.modelo.Perfil;

public interface IPerfilDAO extends IDAO<Perfil> {

}
