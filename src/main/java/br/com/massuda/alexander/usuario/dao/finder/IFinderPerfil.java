package br.com.massuda.alexander.usuario.dao.finder;

import br.com.massuda.alexander.persistencia.IFinder;
import br.com.massuda.alexander.usuario.orm.modelo.Perfil;

public interface IFinderPerfil extends IFinder<Long, Perfil> {

}
