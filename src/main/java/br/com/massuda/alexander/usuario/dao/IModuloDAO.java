package br.com.massuda.alexander.usuario.dao;

import br.com.massuda.alexander.persistencia.IDAO;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Modulo;

public interface IModuloDAO extends IDAO<Modulo> {

}
