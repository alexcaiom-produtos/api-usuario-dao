package br.com.massuda.alexander.usuario.dao.finder;

import br.com.massuda.alexander.persistencia.IFinder;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.autorizacao.Permissao;

public interface IFinderPermissao extends IFinder<Long, Permissao> {
	
	Permissao pesquisarPorUsuario (Long idUsuario);
	
}
