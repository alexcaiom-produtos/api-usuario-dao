package br.com.massuda.alexander.usuario.dao.impl;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.usuario.dao.IFuncionalidadeDAO;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Funcionalidade;

@Component
public class FuncionalidadeDAOImpl extends DAO<Funcionalidade>
							implements IFuncionalidadeDAO {

	public FuncionalidadeDAOImpl() {
		super(Funcionalidade.class);
	}
	
}
