package br.com.massuda.alexander.usuario.dao.usuario.profissional.impl;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.usuario.dao.impl.DAO;
import br.com.massuda.alexander.usuario.dao.usuario.profissional.IEmpresaDAO;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.Empresa;

@Component
public class EmpresaDAOImpl extends DAO<Empresa> implements IEmpresaDAO {

	public EmpresaDAOImpl() {
		super(Empresa.class);
	}

}
