package br.com.massuda.alexander.usuario.dao.finder;

import br.com.massuda.alexander.persistencia.IFinder;
import br.com.massuda.alexander.usuario.orm.modelo.sistema.Modulo;

public interface IFinderModulo extends IFinder<Long, Modulo> {
	
}
