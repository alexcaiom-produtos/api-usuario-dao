package br.com.massuda.alexander.usuario.dao.usuario.profissional;

import br.com.massuda.alexander.persistencia.IDAO;
import br.com.massuda.alexander.usuario.orm.modelo.usuario.profissional.ExperienciaProfissional;

public interface IExperienciaProfissionalDAO extends IDAO<ExperienciaProfissional> {

}
